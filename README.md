*Modified fork of the modest Discourse theme by [MeghnaAJ](https://github.com/MeghnaAJ/discourse-modest-theme)*

# Very Modest Theme

[Demo](https://discourse.theme-creator.io/theme/mononym/very-modest-theme)

![screenshot](./very-modest-theme-screenshot.webp)

[:tada: 2024 #3 Most Popular Discourse Theme Topic](https://meta.discourse.org/t/2024-the-year-in-review/345172/14#p-1678775-most-popular-4)

## Additional Settings

| Setting | Value |
| --- | --- |
| logo | '' |
| logo_small | '' |
| categories_topics | '0' |
| suggested_topics | '0' |
| top_menu | latest\|new\|unread\|categories\|hot |
| post_menu | reply\|like\|edit\|bookmark\|delete\|admin\|read\|flag |
| post_menu_hidden_items | '' |
| enable_badges | 'false' |
| base_font | open_sans |
| heading_font | roboto_mono |
| prioritize_username_in_ux | 'false' |
| enable_user_directory | 'false' |
| show_pinned_excerpt_mobile | 'false' |
| show_pinned_excerpt_desktop | 'false' |
| display_name_on_posts | 'true' |
| email_accent_bg_color | "#3d3846" |
| email_link_color | "#000000" |
| external_system_avatars_url | https://api.dicebear.com/6.x/shapes/svg?seed={username} |
| enable_powered_by_discourse | 'false' |
| tagging_enabled | 'false' |
| chat_separate_sidebar_mode | always |
| presence_enabled | 'false' |

## License

This theme is licensed under the GNU Affero General Public License v3.0 or later.
MeghnaAJ's code is licensed under the MIT License.
